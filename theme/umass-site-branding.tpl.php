<?php 
/**
* 
* @file
* UMass Site Branding template file
* 
* Variables
*
* $items array variable, for example: $items['site_name']
*
* site_name
* site_url
*
* site_parent_name
* site_parent_url
*
* umass_site_branding_option_1 => 
*   umass-site-branding-color Disabled (0/false/default), Enabled (1/true)
* umass_site_branding_option_2 => 
*   umass-site-branding-font Disabled (0/false/default), Enabled (1/true)
* umass_site_branding_option_3 => 
*   umass-site-branding-reverse Disabled (0/false/default), Enabled (1/true)
*
* CSS Classes
*
* umass-site-branding-site
* umass-site-branding-parent
* umass-site-branding-color
* umass-site-branding-font
* umass-site-branding-reverse
*
*/
?>

<?php
// fetch the veggies
$items = $variables['items'];
?>
<?php if ($items['site_parent_name']): ?>
	<div class="umass-site-branding-parent<?php print ($items['umass_site_branding_option_3']?' umass-site-branding-reversed':''); ?>">
	<?php if ($items['site_parent_url']): ?>
		<a href="<?php print render($items['site_parent_url']); ?>">
	<?php endif; ?>
	<?php print render($items['site_parent_name']); ?>
	<?php if ($items['site_parent_url']): ?>
		</a>
	<?php endif; ?>
	</div>
<?php endif; ?>
<div class="umass-site-branding-site<?php print ($items['umass_site_branding_option_1']?' umass-site-branding-color':''); ?><?php print ($items['umass_site_branding_option_2']?' umass-site-branding-font':''); ?><?php print ($items['umass_site_branding_option_3']?' umass-site-branding-reversed':''); ?>">
<a href="<?php print render($items['site_url']); ?>"><?php print render($items['site_name']); ?></a>
</div>
