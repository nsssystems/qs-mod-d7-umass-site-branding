# README #

UMass Site Branding

Enable the umass_site_branding module. 
Set site parent name at Basic Site Settings /admin/config/system/site-information.
Place 'UMass Site Branding' block /admin/structure/block and disable 'Display title'.
Configure block for branding display options.

Authors

David Ruderman <david.ruderman@umass.edu>